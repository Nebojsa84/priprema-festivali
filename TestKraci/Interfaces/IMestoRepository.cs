﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestKraci.Models;

namespace TestKraci.Interfaces
{
     public interface IMestoRepository
    {
        IEnumerable<Mesto> GetAll();
        Mesto GetById(int id);
        void Crate(Mesto mesto);
        void Delete(Mesto mesto);
        void Update(Mesto mesto);
    }
}
