﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestKraci.Models;

namespace TestKraci.Interfaces
{
    public interface IFestivalRepository
    {
        IEnumerable<Festival> GetAll();
        Festival GetById(int id);
        void Create(Festival festival);
        void Delete(Festival festival);
        void Update(Festival festival);
    }
}
