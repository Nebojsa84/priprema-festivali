﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestKraci.Interfaces;

namespace TestKraci.Controllers
{
    public class MestaController : ApiController
    {
        public IMestoRepository _repository { get; set; }

        public MestaController(IMestoRepository repository)
        {

            _repository = repository;
        }

        public IHttpActionResult GetMesta()
        {
            return Ok(_repository.GetAll());
           
        }

        public IHttpActionResult GetMesto(int id)
        {
            var mesto = _repository.GetById(id);
            if (mesto==null)
            {
                return NotFound();
            }
            return Ok(mesto);
        }

        public IHttpActionResult GetMesta(int kod)
        {
            var mesta = _repository.GetAll()
                        .Where(m =>m.PostanskiBroj<kod)
                        .OrderBy(m=>m.PostanskiBroj);

            return Ok(mesta);
        }

    }
}
