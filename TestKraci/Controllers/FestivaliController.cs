﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestKraci.Interfaces;
using TestKraci.Models;

namespace TestKraci.Controllers
{
    public class FestivaliController : ApiController
    {

       private IFestivalRepository _repository { get; set; }

        public FestivaliController(IFestivalRepository repository)
        {
            _repository = repository;
        }

        public IHttpActionResult GetFestivali()
        {
            return Ok(_repository.GetAll());
        }

        public IHttpActionResult GetFestival(int id)
        {
            var festival = _repository.GetById(id);
            if (festival==null)
            {
                return NotFound();
            }

            return Ok(festival);
        }

        [Authorize]
        public IHttpActionResult PostFestival(Festival festival)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Create(festival);
            return CreatedAtRoute("DefaultApi",new { festival.Id},festival);
        }
        [Authorize]
        public IHttpActionResult DeleteFestival(int id)
        {
            var festival = _repository.GetById(id);
            if (festival==null)
            {
                return NotFound();
            }
            _repository.Delete(festival);
            return Ok();

        }
        [Authorize]
        public IHttpActionResult PutFestival(int id, Festival festival)
        {
            if (id!=festival.Id)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                _repository.Update(festival);
            }
            catch 
            {

                throw;
            }

            return Ok(festival);
        }
        [Authorize]
        [Route("api/festivali/pretraga")]
        public IHttpActionResult PostFestival([FromUri]int start, int kraj)
        {
            var festivali = _repository.GetAll()
                            .Where(f => f.Godina > start && f.Godina < kraj)
                            .OrderBy(f => f.Godina);

            if (festivali==null)
            {
                return NotFound();
            }

            return Ok(festivali);
        }
    }
}
