﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TestKraci.Models
{
    public class Festival
    {
        public int Id { get; set; }
        [Required]
        public string Naziv { get; set; }
        [Range(0,9999999999.99)]
        public decimal CenaKarte { get; set; }
        [Range(1951,2017)]
        public int Godina { get; set; }
        public int MestoId { get; set; }
        public Mesto Mesto { get; set; }

    }
}