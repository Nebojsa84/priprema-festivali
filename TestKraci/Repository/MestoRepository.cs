﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using TestKraci.Interfaces;
using TestKraci.Models;

namespace TestKraci.Repository
{
    public class MestoRepository : IDisposable, IMestoRepository
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public void Crate(Mesto mesto)
        {
            db.Mesta.Add(mesto);
            db.SaveChanges();
        }

        public void Delete(Mesto mesto)
        {
            db.Mesta.Remove(mesto);
            db.SaveChanges();
        }


        public IEnumerable<Mesto> GetAll()
        {
            return db.Mesta;
        }

        public Mesto GetById(int id)
        {
            return db.Mesta.FirstOrDefault(m => m.Id == id);
        }

        public void Update(Mesto mesto)
        {
            db.Entry(mesto).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }
            
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}