﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using TestKraci.Interfaces;
using TestKraci.Models;

namespace TestKraci.Repository
{
    public class FestivalRepository : IDisposable,IFestivalRepository
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public void Create(Festival festival)
        {
            db.Festivali.Add(festival);
            db.SaveChanges();

        }

        public void Delete(Festival festival)
        {
            db.Festivali.Remove(festival);
            db.SaveChanges();
        }


        public IEnumerable<Festival> GetAll()
        {
           return db.Festivali.Include(f => f.Mesto)
                    .OrderByDescending(f=>f.CenaKarte);
        }

        public Festival GetById(int id)
        {
            return db.Festivali.FirstOrDefault(f=>f.Id==id);
        }

        public void Update(Festival festival)
        {
            db.Entry(festival).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}