namespace TestKraci.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using TestKraci.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<TestKraci.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TestKraci.Models.ApplicationDbContext context)
        {
             context.Mesta.AddOrUpdate(x => x.Id,
             new Mesto() { Id = 1, Naziv = "Mladenovo",PostanskiBroj=12345 },
             new Mesto() { Id = 2, Naziv = "Backa Palanka" ,PostanskiBroj = 11000},
             new Mesto() { Id = 3, Naziv = "Novi Sad",PostanskiBroj=25147 }
             );

            context.Festivali.AddOrUpdate(x => x.Id,
             new Festival()
             {
                 Id = 1,
                 Naziv = "Pride and Prejudice",
                 Godina = 1959,
                 MestoId = 1,
                 CenaKarte = 9.99M,
                 
                
             },
             new Festival()
             {
                 Id = 2,
                 Naziv = "Northanger Abbey",
                 Godina = 1960,
                 MestoId = 1,
                 CenaKarte = 12.95M,
                
             },
             new Festival()
             {
                 Id = 3,
                 Naziv = "David Copperfield",
                 Godina = 1999,
                 MestoId = 2,
                 CenaKarte = 15,
                 
             },
             new Festival()
             {
                 Id = 4,
                 Naziv = "Don Quixote",
                 Godina = 2017,
                 MestoId = 3,
                 CenaKarte = 8.95M,
                 
             }
             );

        }
    }
}
