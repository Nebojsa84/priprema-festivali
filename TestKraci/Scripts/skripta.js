﻿$(document).ready(function () {

	var host = window.location.host;
	var token = null;
	var headers = {};

	$("#prijava").css("display", "block");
	$("#registracija").css("display", "none");
    $("#pretraga").css("display", "none");
    $("#info").css("display", "none");
    $("#unos").css("display", "none");

	$("body").on("click", "#delete-btn", deleteFestival); 

	showData();

    //POPUNJAVA I PRIKAZUJE TABELU
	function showData() {
		$.ajax({
			type: "GET",
			url: "http://" + host + "/api/Festivali/"
		}).done(showMesta);
	}

    //KREIRANJE TABELE
	function showMesta(data) {

		console.log(data);
		var container = $("#table-div");
		container.empty();

		var table = $("<table/>").addClass("table table-bordered table-striped");
		var tableHeader = $("<thead><tr><th>Naziv</th><th>Mesto</th><th>Cena</th></tr><thead>");
		var tableBody = $("<tbody/>");
		table.append(tableHeader);

		var i;
		for (i = 0; i < data.length; i++) {
			var row = $("<tr/>");
			row.append("<td>" + data[i].Naziv + "</td><td>" + data[i].Mesto.Naziv + "</td><td>" + data[i].CenaKarte + "</td>");
			if (token) {
				row.append("<td><button class='btn btn-danger' id='delete-btn' name=" + data[i].Id + " >Obrisi</button></td>");
			}
			tableBody.append(row);
		}

		table.append(tableBody);
		container.append(table);

    }

    //PRIJAVA
	$("#prijava button").click(function (e) {

		e.preventDefault();

        //POSTO FORMA IMA 2 DUGMETA PROVERAVA NA KOJE JE KLIKNUTO
		if ($(this).attr("id") === "reg-btn") {

			$("#prijava").css("display", "none");
			$("#registracija").css("display", "block");

		}

		if ($(this).attr("id") === "prijava-btn") {

			var userName = $("#priEmail").val();
			var password = $("#priLoz").val();

			var sendData = {

				"grant_type": "password",
				"username": userName,
				"password": password

			};

			$.ajax({
				"type": "POST",
				"url": "http://" + host + "/Token",
				"data": sendData

			}).done(function (data) {
                token = data.access_token;
                $("#priEmail").val("");
                $("#priLoz").val("");
				$("#info-korisnik").text("Prijavljen korisnik: " + data.userName);
                $("#prijava").css("display", "none");
                $("#pretraga").css("display", "block");
                $("#info").css("display", "block");
                $("#unos").css("display", "block");
                fillSelect();
				showData();
			}).fail(function (data) {
				console.log(data);
				alert("Doslo je do greske!");

			});
		}

	});

    //REGISTRACIJA KORISNIKA
	$("#registracija").submit(function (e) {

		e.preventDefault();

		var email = $("#regEmail").val();
		var password = $("#regLoz").val();
		var confirmed = $("#regLoz2").val();

		sendData = {

			"Email": email,
			"Password": password,
			"ConfirmPassword": confirmed
		};

		$.ajax({
			type: "POST",
			url: "http://" + host + "/api/Account/Register",
			data: sendData
		}).done(function (data) {
			console.log(data);
			$("#prijava").css("display", "block");
			$("#registracija").css("display", "none");

		}).fail(function (data) {
			console.log(data);
			alert("Doslo je do greske prilikom registracije");

		});

	});

    //BRISANJE
	function deleteFestival() {

		var festivalId = this.name;
		if (token) {
			headers.Authorization = "Bearer " + token;
		}

		$.ajax({
			"type": "DELETE",
			"url": "http://" + host + "/api/festivali/" + festivalId,
			"headers": headers
		}).done(function (data) {

			showData();
		});
    }
    //ODJAVA KORISNIKA
    $("#odjava-btn").click(function () {

        token = null;
        headers = {};
        $("#prijava").css("display", "block");
        $("#registracija").css("display", "none");
        $("#pretraga").css("display", "none");
        $("#info").css("display", "none");
        $("#unos").css("display", "none");

    });


    //PRETRAGA

    $("#pretraga-frm").submit(function (e) {
        e.preventDefault();

        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        var start = $("#start").val();
        var kraj = $("#kraj").val();
        if (start>kraj) {
            alert("Polje start ne moze biti veé od polja kraj!");
            $("#start").val("");
            $("#kraj").val("");
        }

        if (start<1950 && kraj<2018) {
            alert("Polje start ne moze biti veé od polja kraj!");
            $("#start").val("");
            $("#kraj").val("");

        }
     
        $.ajax({

            "type": "POST",
            "url": "http://" + host + "/api/festivali/pretraga/?start=" + start + "&kraj=" + kraj,
            "headers":headers
        }).done(function (data) {

            showMesta(data);

        });
        

    });

    // POPUNJAVA PADAJUCI MENI MESTIMA
    function fillSelect() {

        $.ajax({
            "type": "GET",
            "url": "http://" + host + "/api/mesta"
        }).done(function (data) {
            var sel = $("#mesta-select");
            console.log(data);
            var i;
            for (i = 0; i < data.length; i++) {

                sel.append('<option value=' + data[i].Id + '>' + data[i].Naziv +'</option>');
            }

        });

    }
    //UNOS NOVOG FESTIVALA
    $("#unos").submit(function (e) {
        e.preventDefault();

        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        var naziv = $("#naziv-fes").val();
        var cena = $("#cena-karte").val();
        var prvoOdrzavanje = $("#prvo-odrz").val();
        var mesto = $("#mesta-select").val();

        if (prvoOdrzavanje < 1950 && prvoOdrzavanje < 2018) {
            alert("Godina mora biti izmedju 1950 i 2018!");
            $("#prvo-odrz").val("");
            
        }

        var sendData = {
            "Naziv": naziv,
            "CenaKarte": cena,
            "Godina": prvoOdrzavanje,
            "MestoId": mesto
        };

        $.ajax({
            "type": "POST",
            "url": "http://" + host + "/api/festivali",
            "data": sendData,
            "headers": headers
        }).done(function (data) {

            showData();
            $("#naziv-fes").val("");
            $("#cena-karte").val("");
            $("#prvo-odrz").val("");
            }).fail(function () {
                alert("Doslo je do greske!");
        });

    });

    //CISTI FORMU ZA UNOS
    $("#odustani-btn").click(function () {

        $("#naziv-fes").val("");
        $("#cena-karte").val("");
        $("#prvo-odrz").val("");

    });

});