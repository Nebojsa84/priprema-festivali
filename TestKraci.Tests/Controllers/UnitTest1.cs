﻿using System;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TestKraci.Controllers;
using TestKraci.Interfaces;
using TestKraci.Models;

namespace TestKraci.Tests.Controllers
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetReturnsObjectWithSameId()
        {
            var repository = new Mock<IFestivalRepository>();
            repository.Setup(x => x.GetById(42)).Returns(new Festival { Id = 42 });

            var controller = new FestivaliController(repository.Object);

            IHttpActionResult actionResult = controller.GetFestival(42);
            var contentResult = actionResult as OkNegotiatedContentResult<Festival>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.Id);

        }

        [TestMethod]

        public void GetReturnsNotFound()
        {
            var repository = new Mock<IFestivalRepository>();
            var controller = new FestivaliController(repository.Object);

            IHttpActionResult actionResult = controller.GetFestival(10);

            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
        [TestMethod]
        public void DeleteReturnsOk()
        {
            var repository = new Mock<IFestivalRepository>();
            repository.Setup(x => x.GetById(10)).Returns(new Festival {Id=10 });
            var controller = new FestivaliController(repository.Object);

            IHttpActionResult actionResult = controller.DeleteFestival(10);

            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }

        [TestMethod]
        public void PostReturnsCreatedAndObject()
        {
            var repository = new Mock<IFestivalRepository>();
            var controler = new FestivaliController(repository.Object);

            IHttpActionResult actionResult = controler.PostFestival(new Festival { Id = 10, CenaKarte = 100, MestoId = 3, Naziv = "Test" });
            var result = actionResult as CreatedAtRouteNegotiatedContentResult<Festival>;

            Assert.IsNotNull(result.Content);
            Assert.AreEqual("DefaultApi", result.RouteName);
            Assert.AreEqual(10, result.Content.Id);
            
        }
    }
}
